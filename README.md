# estate_management(物业管理系统)

## 介绍

本项目为学校大二卓越项目的具体实现。整体实现了一个小区物业管理系统的基础操作，但值得注意的是，并不具备适应现实生活中真实场景的能力。因此仅供学习。

![avatar](./project_img/1.png)



![2](./project_img/2.png)

![3](./project_img/3.png)



**在后端项目中，本项目实现了具体的RBAC权限模型，为了方便演示已关闭相关功能，开启需要前往「PermissionService」类将注释的代码启用。**



其二，系统中集成了基于Quartz的定时任务管理模块，可通过接口在线管理定时任务的启用与停止。

![](./project_img/4.png)

## 整体

本项目使用前后端分离架构，因此分为两个部分。
- em_server 以Springboot为基础的后端项目
- em_ui 以Vue.js为基础的前端项目

其中后端项目中的主要技术选型如下：
- SpringBoot 项目主体
- SpringSecurity 安全框架
- MybatisPlus 数据库框架
- Quartz 定时任务
- EasyExcel Excel表操作

前端项目主要技术选型则是：
- Vue2 项目主体
- [Fantastic-admin](https://hooray.gitee.io/fantastic-admin/guide/start.html) 后台脚手架 因此前端想要更改或者出现问题，可以首先阅读文档：
- Axios.js 请求库
- Ant-Desing-Vue UI框架

# 问题解决


### 默认账户

数据库内用户基本密码都是 **123456** 

| 用户名 | 密码 |
|-----|--------|
| admin  | 123456 |


### 前端无法启动

| 报错内容 | error:0308010C:digital envelope routines::unsupported                                  |
|------|----------------------------------------------------------------------------------------|
| 错误原因 | 出现这个错误是因为 node.js V17版本中最近发布的OpenSSL3.0, 而OpenSSL3.0对允许算法和密钥大小增加了严格的限制，可能会对生态系统造成一些影响. |

解决方案：[解决方案1](https://blog.csdn.net/zjjxxh/article/details/127173968)
解决方案：[解决方案2](https://blog.csdn.net/itopit/article/details/127280592)





### 前端无法正常显示（代码问题已解决）


1.首先确认后端正常开启（重要，重要，重要)，评论之中很多人的原因都是因为后端没启动从而导致的无法显示，因为前端页面启动之时会像后端发起请求查询权限。

2.使用**yarn install** 进行安装第三方库。

### error in ./src/assets/styles/reset.scss


使用**yarn install** 进行安装第三方库。


### 前端框架配置相关

如：页面标题、侧边栏是否收起、是否全屏、主题配置，都在「./src/settings.js 」之中配置。
因此，无论是碰到问题或想要修改，请阅读文档：[Fantastic-admin](https://hooray.gitee.io/fantastic-admin/guide/start.html)

# 修复日志

- 前端：由「router/index.js」 中路由错误，导致的无法展示页面.

- 前端：由「登录代码缺失」，导致的错误。



# 最后

觉得有所帮助，请拿钱打我。😁

![收款码](./project_img/收款码.png)

